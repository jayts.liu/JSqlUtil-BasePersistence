/* Copyright (C) 2018 JayLiu to Present. - All Rights Reserved. */
package test;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

import com.jay.base.annotation.Column;
import com.jay.base.annotation.SqlExclude;
import com.jay.base.dao.AbstractBaseDao;
import com.jay.base.model.AbstractBaseTable;


/**
 * Example using AbstractBaseDao
 * @author JayLiu (JayTs.Liu@gmail.com)
 * @Since 20180509
 * @version 0.1
 */
public class Example extends AbstractBaseDao {
	
	public Example() {
		
		CustomersTest customersTest = new CustomersTest("1", "ABC", "Jay", "MOON", BigDecimal.ZERO);//init data
		
		insert(customersTest);
		print( select1() );
		print( select2() );
		update(new CustomersTest("2", "ABC2", "Jay2", "MOON2", BigDecimal.ZERO));
		
		//sudo: delete(Object myObject):Integer 
	}
	
	public int insert(CustomersTest customersTest) {
		return getSqlUtil().insert(customersTest);
	}
	
	public List<CustomersTest> select1() {
		String sql = "SELECT * FROM CUSTOMERSTEST "; //init query
		return getSqlUtil().find(sql, CustomersTest.class);
	}
	
	public CustomersTest select2() {
		String sql = "SELECT top 1 * FROM CUSTOMERSTEST WEHRE id= ? AND USER_NAME = ? "; //init query
		return getSqlUtil().findOne(sql, CustomersTest.class, "1", "Jay");
	}
	
	public int update(CustomersTest customersTest) {
		return getSqlUtil().update(customersTest);
	}

	//public int delete(Object myObject) {... 
	
	public static void main(String[] args) {
		new Example();
	}
	
	private void print(List<CustomersTest> cusList) {
		for(CustomersTest cus: cusList)
			print(cus); //print out
	}
	
	private void print(CustomersTest cus) {
		System.out.println(cus.toString()); //print out
	}
}

/*
example schema

CREATE TABLE [CUSTOMERSTEST](
	[ID] [varchar](25) NULL,
	[CUS_CODE] [varchar](25) NULL,
	[CUS_NAME] [varchar](25) NULL,
	[HADDRESS] [varchar](100) NULL,
	[PRICE] [float] NULL
) ON [PRIMARY]
 */
class CustomersTest extends AbstractBaseTable {//model

	private final static String TABLE_NAME = "CUSTOMERSTEST";
	
	@SqlExclude  //example to skip this column for query
	private String id;
	
	@Column(name="CUS_CODE") //example annotation to specify table column name
	private String cusCode;
	
	private String cusName, haddress;
	private BigDecimal price;
	
	public CustomersTest() {}

	public CustomersTest(String id, String cusCode, String cusName, String haddress, BigDecimal price) {
		super();
		this.id = id;
		this.cusCode = cusCode;
		this.cusName = cusName;
		this.haddress = haddress;
		this.price = price;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCusCode() {
		return cusCode;
	}

	public void setCusCode(String cusCode) {
		this.cusCode = cusCode;
	}

	public String getCusName() {
		return cusName;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	public String getHaddress() {
		return haddress;
	}

	public void setHaddress(String haddress) {
		this.haddress = haddress;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	
	public String getTableName() {
		return TABLE_NAME;
	}

	@Override
	public Set<String> getKeyNames() {
		return null;
	}

	@Override
	public String toString() {
		return "CustomersTest [id=" + id + ", cusCode=" + cusCode + ", cusName=" + cusName + ", haddress=" + haddress
				+ ", price=" + price + "]";
	}

}

