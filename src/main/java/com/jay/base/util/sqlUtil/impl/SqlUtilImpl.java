/**
 * Copyright 2018 Jay Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jay.base.util.sqlUtil.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.jay.base.database.jdbc.JdbcTemplateEx;
import com.jay.base.database.jdbc.SimpleJdbcCallEx;
import com.jay.base.database.jdbc.SimpleJdbcInsertEx;
import com.jay.base.database.jdbc.mapper.JBaseMapper;
import com.jay.base.model.BaseStoreProcedure;
import com.jay.base.model.BaseTable;
import com.jay.base.model.QueryType;
import com.jay.base.util.model.ObjectDataExtractor;
import com.jay.base.util.model.ObjectFieldsScanner;
import com.jay.base.util.parser.SPHelper;
import com.jay.base.util.sqlUtil.SqlUtil;


/**
 * For spring JdbcTemplate
 * @author JayLiu (JayTs.Liu@gmail.com)
 * @Since 2018
 * @update 20181225
 * @version 1.7.1
 */
public class SqlUtilImpl implements SqlUtil {
	
	private JdbcTemplateEx jdbcTemplateEx;
	private SimpleJdbcInsertEx simpleJdbcInsertEx;
	private SimpleJdbcCall simpleJdbcCall;
	
	public SqlUtilImpl() {}
	
	public void init(JdbcTemplateEx jdbcTemplateEx) {
		this.jdbcTemplateEx = jdbcTemplateEx;
		this.simpleJdbcInsertEx = new SimpleJdbcInsertEx(jdbcTemplateEx);
		this.simpleJdbcCall = new SimpleJdbcCallEx(jdbcTemplateEx);
	}
	
	@Override
	public void init(JdbcTemplateEx jdbcTemplateEx, SimpleJdbcInsertEx simpleJdbcInsertEx) {
		this.jdbcTemplateEx = jdbcTemplateEx;
		this.simpleJdbcInsertEx = simpleJdbcInsertEx;
	}
	
	@Override
	public void init(JdbcTemplateEx jdbcTemplateEx, SimpleJdbcInsertEx simpleJdbcInsertEx, SimpleJdbcCall simpleJdbcCall) {
		this.jdbcTemplateEx = jdbcTemplateEx;
		this.simpleJdbcInsertEx = simpleJdbcInsertEx;
		this.simpleJdbcCall = simpleJdbcCall;
	}
	
	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public <E> E queryForObject(String sql, Class<? extends E> clazz, Object... params) {
		return (E) jdbcTemplateEx.queryForObject( sql, params, new BeanPropertyRowMapper(clazz)); //20180704 update for Incorrect result size: expected 1, actual 0
	}
	
	@Override
	public <E> Map<String, Object> queryForMap(String sql, Object... params) { //20180712 add
		return jdbcTemplateEx.queryForMap(sql, params);
	}
	
	@Override
	public <E> List<Map<String, Object>> queryForList(String sql, Object... params) { //20180712 add
		return jdbcTemplateEx.queryForList(sql, params);
	}
	
	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public <E> E findOne(String sql, Class<? extends E> clazz, Object... params) {
		List lst = find(sql, clazz, params);
		//List lst = jdbcTemplateEx.query( sql, params, new BeanPropertyRowMapper(clazz)); //20180711 update to avoid null cause
		if (lst.isEmpty())
			return null;
	    else
	    	return (E) lst.get(0);
	}
	
	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public <E> List<E> find(String sql, Class<E> clazz, Object... params) {
		return jdbcTemplateEx.query( sql, params, (ObjectFieldsScanner.isColumnAnnoExist(clazz) ? new JBaseMapper(clazz) : new BeanPropertyRowMapper(clazz)));
		//return jdbcTemplateEx.query( sql, params, new BeanPropertyRowMapper(clazz)); //20180711 update
	}
	
	@Override
	public int update(String sql, Object... params) {
		return jdbcTemplateEx.update(sql, params);
	}
	
	@Override
	public int insertFilterExclude(BaseTable object) {
		return jdbcTemplateEx.insert(object);
	}
	
	@Override
	public int insert(BaseTable object) {
		simpleJdbcInsertEx.insert(object);
		return 1;
	}
	
	@Override
	public Number insertAndReturnKey(BaseTable object) {
		return simpleJdbcInsertEx.insertAndReturnKey(object);
	}
	
	@Override
	public int update(BaseTable object) {
		return jdbcTemplateEx.update(object);
	}

	@Override
	public int delete(BaseTable object) {
		return jdbcTemplateEx.delete(object);
	}
	
	@Override
	public int[] updateBatch(List<String> sqlList) {
		return jdbcTemplateEx.updateBatch(sqlList);
	}
	
	@Override
	public int[] updateBatch(QueryType queryType, List<?> objectList) {
		return jdbcTemplateEx.updateBatch(queryType, objectList);
	}

	@Deprecated
	public boolean updateBatch(String sql, List<Object[]> paramsLst) {
		return false;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public <E> E executeSp(BaseStoreProcedure storeProcedure, Class<E> clazz) {
		
		SPHelper spHelper = new SPHelper(storeProcedure);
		SqlParameterSource parameters = null;
		
		try {
			parameters = ObjectFieldsScanner.isSpAnnoExist(storeProcedure.getClass()) 
					? new MapSqlParameterSource(new ObjectDataExtractor(storeProcedure).getMap()) 
					: new BeanPropertySqlParameterSource(storeProcedure);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		simpleJdbcCall.withProcedureName(spHelper.getStoreProcedureName());
		simpleJdbcCall.returningResultSet("result_cursor", (ObjectFieldsScanner.isColumnAnnoExist(clazz) ? new JBaseMapper(clazz) : BeanPropertyRowMapper.newInstance(clazz)));
		
		E result = null;
		Map<String, Object> resultMap = simpleJdbcCall.execute(parameters);
		if(resultMap!=null) {
			List<E> lst = (List<E>) resultMap.get("result_cursor");
			if(lst != null && lst.size() > 0)
				result = lst.get(0);
		}
		return result;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public <E> List<E> executeSpRetrieveList(BaseStoreProcedure storeProcedure, Class<E> clazz) {
		
		SPHelper sp = new SPHelper(storeProcedure);
//		SqlParameterSource parameters = new BeanPropertySqlParameterSource(baseStoreProcedure);
		SqlParameterSource parameters = null;
		
		try {
			parameters = ObjectFieldsScanner.isSpAnnoExist(storeProcedure.getClass()) 
					? new MapSqlParameterSource(new ObjectDataExtractor(storeProcedure).getMap()) 
					: new BeanPropertySqlParameterSource(storeProcedure);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		simpleJdbcCall.withProcedureName(sp.getStoreProcedureName());
		simpleJdbcCall.returningResultSet("result_cursor", (ObjectFieldsScanner.isColumnAnnoExist(clazz) ? new JBaseMapper(clazz) : BeanPropertyRowMapper.newInstance(clazz)));
		
		Map<String, Object> reultMap = simpleJdbcCall.execute(parameters);
		return (List<E>) reultMap.get("result_cursor");
	}
	
	public Map<String, Object> executeSpRetrieveMap(BaseStoreProcedure storeProcedure) {
		SPHelper sp = new SPHelper(storeProcedure);
		SqlParameterSource parameters = null;
		
		try {
			parameters = ObjectFieldsScanner.isSpAnnoExist(storeProcedure.getClass()) 
					? new MapSqlParameterSource(new ObjectDataExtractor(storeProcedure).getMap()) 
					: new BeanPropertySqlParameterSource(storeProcedure);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		simpleJdbcCall.withProcedureName(sp.getStoreProcedureName());
		
		return simpleJdbcCall.execute(parameters);
	}

	@Override
	public JdbcTemplateEx getJdbcTemplate() {
		return jdbcTemplateEx;
	}
	
	@Override
	public void setJdbcTemplate(JdbcTemplateEx jdbcTemplateEx) {
		this.jdbcTemplateEx = jdbcTemplateEx;
	}

	@Override
	public SimpleJdbcInsertEx getSimpleJdbcInsert() {
		return simpleJdbcInsertEx;
	}
	
	@Override
	public void setSimpleJdbcInsert(SimpleJdbcInsertEx simpleJdbcInsertEx) {
		this.simpleJdbcInsertEx = simpleJdbcInsertEx;
	}
	
	@Override
	public SimpleJdbcCall getSimpleJdbcCall() {
		return simpleJdbcCall;
	}

	@Override
	public void setSimpleJdbcCall(SimpleJdbcCall simpleJdbcCall) {
		this.simpleJdbcCall = simpleJdbcCall;
	}

}
