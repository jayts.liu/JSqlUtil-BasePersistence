package com.jay.base.dao;

/**
 * Copyright 2018 Jay Liu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import com.jay.base.database.BaseDaoSupport;
import com.jay.base.database.jdbc.JdbcTemplateEx;
import com.jay.base.util.sqlUtil.SqlUtil;
import com.jay.base.util.sqlUtil.impl.SqlUtilImpl;

/**
 * 
 * @author JayLiu (JayTs.Liu@gmail.com)
 * @since 20180507
 * @update add getSimpleJdbcCall
 * @version 0.2
 */
public abstract class AbstractBaseDao extends BaseDaoSupport {
	
	
	private static SqlUtil sqlUtil;
	
	public SqlUtil getSqlUtil() {
		if(this.sqlUtil == null) 
			this.sqlUtil = new SqlUtilImpl();
		
		JdbcTemplateEx jdbcTemplate = getJdbcTemplate();
//		if(jdbcTemplate!=null) {
//			this.sqlUtil.setJdbcTemplate(jdbcTemplate);
//			SimpleJdbcInsertEx jdbcInsertEx = new SimpleJdbcInsertEx(jdbcTemplate);
//			SimpleJdbcCallEx simpleJdbcCall = new SimpleJdbcCallEx(jdbcTemplate);
//			this.sqlUtil.init(getJdbcTemplate(), jdbcInsertEx, simpleJdbcCall); //20181224 add getSimpleJdbcCall
//		}
		if(jdbcTemplate!=null)
			sqlUtil.init(getJdbcTemplate());
		return this.sqlUtil;
	}
	
	public SqlUtil getSqlUtil(JdbcTemplateEx jdbcTemplate) {
		if(this.sqlUtil == null)
			this.sqlUtil = new SqlUtilImpl();
		this.sqlUtil.init(jdbcTemplate);
		return this.sqlUtil;
	}
}
