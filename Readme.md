# JSqlUtil

 Utility for BasePersistence, idea to help programmer simplify the work and save time for focus on ER modeling.


### Prerequisites

 The project was tested under windows environment.

 1. Java 1.8+
 2. Spring Framework JDBC 5+
 3. [BasePersistence](http://gitlab.com/jayts.liu/BasePersistence)


### Installing

 1. Ensure BasePersistence is ready in your project.
 2. Place JSqlUtil to your project
 3. Extends abstract AbstractBaseDao.class for further use.
 4. Finished


### Example

 INSERT、UPDATE、DELETE : 
~~~
 CustomersTest customersTest = new CustomersTest("1", "ABC", "Jay", "MOON", BigDecimal.ZERO);//init data
 getSqlUtil().insert(customersTest);
 getSqlUtil().update(customersTest);
 getSqlUtil().delete(customersTest);
~~~

 SELECT Example 1:
~~~
 String sql = "SELECT * FROM CUSTOMERSTEST "; //init query
 getSqlUtil().find(sql, CustomersTest.class);
~~~

 SELECT Example2
~~~
 String sql = "SELECT top 1 * FROM CUSTOMERSTEST WEHRE id= ? AND USER_NAME = ? "; //init query
 getSqlUtil().findOne(sql, CustomersTest.class, "1", "Jay");
~~~

**NOTE:** 
 For more detail please check src/main/java/test/Example.java


### Built With

* [Spring Framework](https://spring.io) - The framework used
* [GRADLE](https://gradle.org/) - Dependency Management
* [BasePersistence](http://gitlab.com/jayts.liu/BasePersistence)

### License

 This project is licensed under the Apache License, Version 2.0 License - see the [LICENSE](LICENSE) file for details


